---
title: About
layout: page
---

![Profile Image]({{ site.url }}/{{ site.picture }})

<p>Hi, I'm Dione <i>(Dee-ahn)</i>! I'm an HR professional with over 4 years of experience in Benefits, HRIS systems, Change Management strategies, Training, and Employment Relations.</p>


<p>I like to make HR concepts easier. That means easier to understand, use, and intergrate. Who doesn't appreciate simplicity?</p>


<h2>Skills</h2>

<ul class="skill-list">
	<li>Benefits Administration</li>
	<li>Communicating with others </li>
	<li>Experience with GitLab</li>
	<li>Experience working in data-driven environment</li>
	<li>Organized self-starter</li>
	<li>Team player</li>
</ul>
